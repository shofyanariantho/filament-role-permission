<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $admin = User::factory()->create([
            'name' => 'Webdev',
            'email' => 'webdev@shofyan.my',
            'password' => '$2y$10$1VRhlyYunDxyToIBV8Nu/eGcYEar/aVD1YFj3aC7cqm1OXvO5u4tK',
        ]);
        
        User::factory()->create([
            'name' => 'Test',
            'email' => 'test@shofyan.my',
            'password' => '$2y$10$1VRhlyYunDxyToIBV8Nu/eGcYEar/aVD1YFj3aC7cqm1OXvO5u4tK',
        ]);
        $role = Role::create(['name' => 'Admin']);
        $admin->assignRole($role);
    }
}
